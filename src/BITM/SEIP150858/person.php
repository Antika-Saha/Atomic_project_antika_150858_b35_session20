<?php
namespace App;


class person
{
    public $name="Antika";
    public $gender="Female";
    public $blood_group="B+";

    public function showPersonInfo()
    {
        echo $this->name."<br>";
        echo $this->gender."<br>";
        echo $this->blood_group."<br>";
    }

}